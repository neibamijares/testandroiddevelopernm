/***************************************/ 
/*	         Movies & Series           */
/***************************************/
/**
 * Descripción
 */
Aplicación realizada para el Coding Challenge de la Prueba técnica para candidatos a Desarrollador Android. 
Ésta es una aplicación que muestra las movies y series en 3 categorías: Popular, Top Rated y Upcoming.

/**
 * Contribution guidelines
 */

minSdkVersion 16
targetSdkVersion 25

/**
 * Retrofit, Okhttp and Otto for API communications
 */
compile 'com.squareup.retrofit2:retrofit:2.1.0'
compile 'com.squareup.retrofit2:converter-gson:2.0.0'
compile 'com.squareup.okhttp3:okhttp:3.5.0'
compile 'com.squareup.okhttp3:logging-interceptor:3.5.0'

/**
 * Rx
 */
compile 'com.squareup.retrofit2:adapter-rxjava:2.0.0'
compile 'io.reactivex:rxandroid:1.2.1'
compile 'io.reactivex:rxjava:1.1.6'

/**
 * Butterknife
 */
compile 'com.jakewharton:butterknife:8.0.1'
apt 'com.jakewharton:butterknife-compiler:8.0.1'

/**
 * Picasso
 */
compile 'com.squareup.picasso:picasso:2.5.2'

/**
 * CircleImageView
 */
compile 'de.hdodenhof:circleimageview:2.1.0'
