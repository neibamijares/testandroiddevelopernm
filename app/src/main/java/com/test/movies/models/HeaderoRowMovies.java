package com.test.movies.models;

import java.util.List;

/**
 * Created by hp on 18/3/2017.
 */

public class HeaderoRowMovies {

    private String header;
    //private ResultCatMovies rows;
    private List<ResultCatMovies> rows;
    private boolean row;

    public static HeaderoRowMovies createRow(List<ResultCatMovies> row) {
        HeaderoRowMovies ret = new HeaderoRowMovies();
        ret.rows = row;
        ret.row = true;
        return ret;
    }

    public static HeaderoRowMovies createHeader(String header) {
        HeaderoRowMovies ret = new HeaderoRowMovies();
        ret.header = header;
        ret.row = false;
        return ret;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<ResultCatMovies> getRows() {
        return rows;
    }

    public void setRows(List<ResultCatMovies> rows) {
        this.rows = rows;
    }

    public boolean isRow() {
        return row;
    }

    public void setRow(boolean row) {
        this.row = row;
    }
}
