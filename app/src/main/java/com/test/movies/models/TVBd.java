package com.test.movies.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.test.movies.data.BDContract;

/**
 * Created by hp on 18/3/2017.
 */

public class TVBd {

    String tv_id;
    String tv_name;
    String tv_poster;
    String tv_overview;
    String tv_rating;
    String tv_date;
    String tv_cat;




    public TVBd(){

    }

    public TVBd(String tv_id, String tv_name, String tv_poster, String tv_overview, String tv_rating, String tv_date, String tv_cat) {
        this.tv_id = tv_id;
        this.tv_name = tv_name;
        this.tv_poster = tv_poster;
        this.tv_overview = tv_overview;
        this.tv_rating = tv_rating;
        this.tv_date = tv_date;
        this.tv_cat = tv_cat;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(BDContract.TV.TV_ID,tv_id);
        values.put(BDContract.TV.TV_CAT,tv_cat);
        values.put(BDContract.TV.TV_NAME,tv_name);
        values.put(BDContract.TV.TV_OVERVIEW,tv_overview);
        values.put(BDContract.TV.TV_POSTER,tv_poster);
        values.put(BDContract.TV.TV_RATING,tv_rating);
        values.put(BDContract.TV.TV_DATE,tv_date);

        return values;
    }



    public TVBd(Cursor cursor) {

        tv_id = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_ID));
        tv_name = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_NAME));
        tv_poster = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_POSTER));
        tv_overview = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_OVERVIEW));
        tv_rating = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_RATING));
        tv_date = cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_DATE));
        tv_cat=cursor.getString(cursor.getColumnIndex(BDContract.TV.TV_CAT));

    }

    public String getTv_id() {
        return tv_id;
    }

    public void setTv_id(String tv_id) {
        this.tv_id = tv_id;
    }

    public String getTv_name() {
        return tv_name;
    }

    public void setTv_name(String tv_name) {
        this.tv_name = tv_name;
    }

    public String getTv_poster() {
        return tv_poster;
    }

    public void setTv_poster(String tv_poster) {
        this.tv_poster = tv_poster;
    }

    public String getTv_overview() {
        return tv_overview;
    }

    public void setTv_overview(String tv_overview) {
        this.tv_overview = tv_overview;
    }

    public String getTv_rating() {
        return tv_rating;
    }

    public void setTv_rating(String tv_rating) {
        this.tv_rating = tv_rating;
    }

    public String getTv_date() {
        return tv_date;
    }

    public void setTv_date(String tv_date) {
        this.tv_date = tv_date;
    }

    public String getTv_cat() {
        return tv_cat;
    }

    public void setTv_cat(String tv_cat) {
        this.tv_cat = tv_cat;
    }
}
