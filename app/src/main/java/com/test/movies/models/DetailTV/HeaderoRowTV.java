package com.test.movies.models.DetailTV;

import com.test.movies.models.ResultCatMovies;
import com.test.movies.models.ResultCatTV;

import java.util.List;

/**
 * Created by hp on 18/3/2017.
 */

public class HeaderoRowTV {

    private String header;
    //private ResultCatMovies rows;
    private List<ResultCatTV> rows;
    private boolean row;

    public static HeaderoRowTV createRow(List<ResultCatTV> row) {
        HeaderoRowTV ret = new HeaderoRowTV();
        ret.rows = row;
        ret.row = true;
        return ret;
    }

    public static HeaderoRowTV createHeader(String header) {
        HeaderoRowTV ret = new HeaderoRowTV();
        ret.header = header;
        ret.row = false;
        return ret;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<ResultCatTV> getRows() {
        return rows;
    }

    public void setRows(List<ResultCatTV> rows) {
        this.rows = rows;
    }

    public boolean isRow() {
        return row;
    }

    public void setRow(boolean row) {
        this.row = row;
    }
}
