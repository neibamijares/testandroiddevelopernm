package com.test.movies.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.test.movies.data.BDContract;

/**
 * Created by hp on 18/3/2017.
 */

public class MoviesBd {

    String movie_id;
    String movie_name;
    String movie_poster;
    String movie_overview;
    String movie_rating;
    String movie_date;
    String movie_cat;




    public MoviesBd(){

    }

    public MoviesBd(String movie_id, String movie_name, String movie_poster, String movie_overview, String movie_rating, String movie_date, String movie_cat) {
        this.movie_id = movie_id;
        this.movie_name = movie_name;
        this.movie_poster = movie_poster;
        this.movie_overview = movie_overview;
        this.movie_rating = movie_rating;
        this.movie_date = movie_date;
        this.movie_cat = movie_cat;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(BDContract.Moview.MOVIE_ID,movie_id);
        values.put(BDContract.Moview.MOVIE_CAT,movie_cat);
        values.put(BDContract.Moview.MOVIE_NAME,movie_name);
        values.put(BDContract.Moview.MOVIE_OVERVIEW,movie_overview);
        values.put(BDContract.Moview.MOVIE_POSTER,movie_poster);
        values.put(BDContract.Moview.MOVIE_RATING,movie_rating);
        values.put(BDContract.Moview.MOVIE_DATE,movie_date);

        return values;
    }



    public MoviesBd (Cursor cursor) {

        movie_id = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_ID));
        movie_name = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_NAME));
        movie_poster = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_POSTER));
        movie_overview = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_OVERVIEW));
        movie_rating = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_RATING));
        movie_date = cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_DATE));
        movie_cat=cursor.getString(cursor.getColumnIndex(BDContract.Moview.MOVIE_CAT));

    }

    public String getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(String movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public String getMovie_poster() {
        return movie_poster;
    }

    public void setMovie_poster(String movie_poster) {
        this.movie_poster = movie_poster;
    }

    public String getMovie_overview() {
        return movie_overview;
    }

    public void setMovie_overview(String movie_overview) {
        this.movie_overview = movie_overview;
    }

    public String getMovie_rating() {
        return movie_rating;
    }

    public void setMovie_rating(String movie_rating) {
        this.movie_rating = movie_rating;
    }

    public String getMovie_date() {
        return movie_date;
    }

    public void setMovie_date(String movie_date) {
        this.movie_date = movie_date;
    }

    public String getMovie_cat() {
        return movie_cat;
    }

    public void setMovie_cat(String movie_cat) {
        this.movie_cat = movie_cat;
    }
}
