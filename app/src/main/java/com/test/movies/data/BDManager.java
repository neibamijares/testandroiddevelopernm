package com.test.movies.data;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;

import com.test.movies.models.MoviesBd;
import com.test.movies.models.TVBd;

import java.util.ArrayList;
import java.util.List;



public class BDManager {

    private static BDHelper DataBase;

    private static BDManager instance = new BDManager();

    private static ContentResolver ContentResolver;

    //  Operation List
    private static ArrayList<ContentProviderOperation> ops;


    private BDManager() {
    }

    public static BDManager getInstance(Context contexto) {
        if (DataBase == null) {
            DataBase = new BDHelper(contexto);
        }

        return instance;
    }


    public static void SaveMovies(Context context, List<MoviesBd> movies){

        ContentResolver = context.getContentResolver();
        ops = new ArrayList<>();

        for (int i=0;i<movies.size();i++){
            ops.add(ContentProviderOperation.newInsert(BDContract.Moview.CONTENT_URI)
                    .withValues(movies.get(i).toContentValues())
                    .build());
        }

        try {
            ContentResolver.applyBatch(BDContract.CONTENT_AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

    }



    public static Cursor AllMovies(Context context){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.Moview.CONTENT_URI, null, null, null, null);
    }

    public static Cursor MoviesByID(Context context, String Id){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.Moview.createUriMovies(Id), null, null, null, null);
    }

    public static Cursor MoviesType(Context context, String type){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.Moview.createUriMoviesTypes(type), null, null, null, null);
    }

    public static Cursor MoviesSearch(Context context, String search){

        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.Moview.createUriMoviesSeacrh(search), null, null, null, null);
    }

    public static void MoviesDelete(Context context, String deletecat){

        ContentResolver = context.getContentResolver();
        ContentResolver.delete(BDContract.Moview.createUriMoviesDelete(deletecat), null, null);


    }

    public static void SaveTV(Context context, List<TVBd> tv){

        ContentResolver = context.getContentResolver();
        ops = new ArrayList<>();

        for (int i=0;i<tv.size();i++){
            ops.add(ContentProviderOperation.newInsert(BDContract.TV.CONTENT_URI)
                    .withValues(tv.get(i).toContentValues())
                    .build());
        }

        try {
            ContentResolver.applyBatch(BDContract.CONTENT_AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

    }



    public static Cursor AllTV(Context context){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.TV.CONTENT_URI, null, null, null, null);
    }

    public static Cursor TVByID(Context context, String Id){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.TV.createUriTV(Id), null, null, null, null);
    }

    public static Cursor SeriesType(Context context, String type){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.TV.createUriTVTypes(type), null, null, null, null);
    }

    public static Cursor TVSearch(Context context, String search){
        ContentResolver = context.getContentResolver();
        return ContentResolver.query(BDContract.TV.createUriTVSeacrh(search), null, null, null, null);
    }



}
