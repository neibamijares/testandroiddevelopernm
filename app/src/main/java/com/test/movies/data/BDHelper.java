package com.test.movies.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by hp on 24/1/2017.
 */

public class BDHelper extends SQLiteOpenHelper {

    private static final String DATA_BASE_NAME = "movie.db";

    private static final int ACTUAL_VERSION = 2;

    private final Context contexto;

    interface Tables {
        String MOVIES = "movies";
        String TV = "tv";
    }


    public BDHelper(Context contexto) {
        super(contexto, DATA_BASE_NAME, null, ACTUAL_VERSION);
        this.contexto = contexto;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(String.format("CREATE TABLE %s " +

                        "(%s INTEGER PRIMARY KEY AUTOINCREMENT," +

                        "%s TEXT NOT NULL," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT)",

                Tables.MOVIES,

                BaseColumns._ID,

                BDContract.MovieColumns.MOVIE_ID,
                BDContract.MovieColumns.MOVIE_CAT,
                BDContract.MovieColumns.MOVIE_NAME,
                BDContract.MovieColumns.MOVIE_POSTER,
                BDContract.MovieColumns.MOVIE_OVERVIEW,
                BDContract.MovieColumns.MOVIE_DATE,
                BDContract.MovieColumns.MOVIE_RATING

        ));

        db.execSQL(String.format("CREATE TABLE %s " +

                        "(%s INTEGER PRIMARY KEY AUTOINCREMENT," +

                        "%s TEXT NOT NULL," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT)",

                Tables.TV,

                BaseColumns._ID,

                BDContract.TV.TV_ID,
                BDContract.TV.TV_CAT,
                BDContract.TV.TV_NAME,
                BDContract.TV.TV_POSTER,
                BDContract.TV.TV_OVERVIEW,
                BDContract.TV.TV_DATE,
                BDContract.TV.TV_RATING

        ));


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Tables.MOVIES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TV);

        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }



}
