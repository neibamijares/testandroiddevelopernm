package com.test.movies.data;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;



import com.test.movies.data.BDHelper.Tables;

import java.util.ArrayList;

public class MovieProvider extends ContentProvider {

    private BDHelper BD;

    private ContentResolver Content_Resolver;

    public static final UriMatcher uriMatcher;

    public static final int MOVIES = 100;
    public static final int MOVIES_ID = 101;
    public static final int MOVIES_ID_TYPE = 102;
    public static final int MOVIES_SEARCH = 103;
    public static final int MOVIES_CATEGORIA = 104;


    public static final int TV = 200;
    public static final int TV_ID = 201;
    public static final int TV_ID_TYPE = 202;
    public static final int TV_SEARCH = 203;


    public static final String AUTHORITY = "com.test.movies";

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(AUTHORITY, "movies", MOVIES);
        uriMatcher.addURI(AUTHORITY, "movies/*", MOVIES_ID);
        uriMatcher.addURI(AUTHORITY, "movies/*/type", MOVIES_ID_TYPE);
        uriMatcher.addURI(AUTHORITY, "movies/*/search", MOVIES_SEARCH);
        uriMatcher.addURI(AUTHORITY, "movies/*/deletecat", MOVIES_CATEGORIA);

        uriMatcher.addURI(AUTHORITY, "tv", TV);
        uriMatcher.addURI(AUTHORITY, "tv/*", TV_ID);
        uriMatcher.addURI(AUTHORITY, "tv/*/type", TV_ID_TYPE);
        uriMatcher.addURI(AUTHORITY, "tv/*/search", TV_SEARCH);

    }

    public MovieProvider() {
    }

    @Override
    public boolean onCreate() {
        BD = new BDHelper(getContext());
        Content_Resolver = getContext().getContentResolver();
        return true;
    }


    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case MOVIES:
                return BDContract.generarMime("movies");
            case MOVIES_ID:
                return BDContract.generarMimeItem("movies");

            case TV:
                return BDContract.generarMime("tv");
            case TV_ID:
                return BDContract.generarMimeItem("tv");

            default:
                throw new UnsupportedOperationException("Unknown Uri =>" + uri);
        }
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {

        final SQLiteDatabase DB = BD.getWritableDatabase();
        DB.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            DB.setTransactionSuccessful();
            return results;
        } finally {
            DB.endTransaction();
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {

        SQLiteDatabase DB = BD.getWritableDatabase();

        String id;
        String sequence;

        switch (uriMatcher.match(uri)) {

            case MOVIES:

                DB.insertOrThrow(Tables.MOVIES, null, values);
                notificationChange(uri);
                return BDContract.Moview.createUriMovies(values.getAsString("movie_id"));


            case TV:

                DB.insertOrThrow(Tables.TV, null, values);
                notificationChange(uri);
                return BDContract.TV.createUriTV(values.getAsString("tv_id"));

            default:
                throw new UnsupportedOperationException("no_support_uri");
        }
    }


    private void notificationChange(Uri uri) {
        Content_Resolver.notifyChange(uri, null);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        SQLiteDatabase DB = BD.getWritableDatabase();
        String id;
        int res;

        switch (uriMatcher.match(uri)) {


            case MOVIES:
                id = BDContract.Moview.getIdMovies(uri);
                res = DB.update(Tables.MOVIES, values,
                        BDContract.Moview.MOVIE_ID + " = ?", new String[]{id});
                notificationChange(uri);
                break;


            case TV:
                id = BDContract.TV.getIdTV(uri);
                res = DB.update(Tables.TV, values,
                        BDContract.TV.TV_ID + " = ?", new String[]{id});
                notificationChange(uri);
                break;

            default:
                throw new UnsupportedOperationException("no_support_uri");
        }

        return res;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase DB = BD.getWritableDatabase();
        String id;
        int res;


        switch (uriMatcher.match(uri)) {

            case MOVIES_ID:

                id = BDContract.Moview.getIdMovies(uri);
                res = DB.delete(
                        Tables.MOVIES,
                        BDContract.Moview.MOVIE_ID + " = ? ",
                        new String[]{id}
                );
                notificationChange(uri);
                break;

            case MOVIES_CATEGORIA:

                id = BDContract.Moview.getIdMovies(uri);
                res = DB.delete(
                        Tables.MOVIES,
                        BDContract.Moview.MOVIE_CAT + " = ? ",
                        new String[]{id}
                );
                notificationChange(uri);
                break;

            case TV_ID:

                id = BDContract.TV.getIdTV(uri);
                res = DB.delete(
                        Tables.TV,
                        BDContract.TV.TV_ID + " = ? ",
                        new String[]{id}
                );
                notificationChange(uri);
                break;

            default:
                throw new UnsupportedOperationException("no_support_uri");
        }
        return res;
    }




    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

//    query (SQLiteDatabase db,
//             String[] projectionIn, ---> columns
//             String selection, ---> where
//             String[] selectionArgs,  --->  where arguments
//             String groupBy,
//             String having,
//             String sortOrder)


        SQLiteDatabase DB = BD.getReadableDatabase();

        int match = uriMatcher.match(uri);

        Cursor c = null;

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        String id;

        String[] claves = {};

        String seleccion = "";


        switch (match) {

            case MOVIES:

                c = DB.query(Tables.MOVIES, projection,
                        selection, selectionArgs,
                        null, null, sortOrder);

            break;

            case MOVIES_ID:


                id = BDContract.Moview.getIdMovies(uri);

                c = DB.query(Tables.MOVIES, projection,
                        BDContract.Moview.MOVIE_ID + "=" + "\'" + id + "\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.Moview.MOVIE_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);
                break;

            case MOVIES_ID_TYPE:

                id = BDContract.Moview.getIdMovies(uri);

                c = DB.query(Tables.MOVIES, projection,
                        BDContract.Moview.MOVIE_CAT + "=" + "\'" + id + "\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.Moview.MOVIE_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);



                break;

            case MOVIES_SEARCH:

                id = BDContract.Moview.getIdMovies(uri);
                c = DB.query(Tables.MOVIES, projection,
                        BDContract.Moview.MOVIE_NAME + " like " + "\'%" + id + "%\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.Moview.MOVIE_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);

                break;

            case TV:

                c = DB.query(Tables.TV, projection,
                        selection, selectionArgs,
                        null, null, sortOrder);

                break;

            case TV_ID:


                id = BDContract.TV.getIdTV(uri);

                c = DB.query(Tables.TV, projection,
                        BDContract.TV.TV_ID + "=" + "\'" + id + "\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.TV.TV_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);
                break;

            case TV_ID_TYPE:

                id = BDContract.TV.getIdTV(uri);

                c = DB.query(Tables.TV, projection,
                        BDContract.TV.TV_CAT + "=" + "\'" + id + "\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.TV.TV_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);

                break;

            case TV_SEARCH:

                id = BDContract.TV.getIdTV(uri);


                c = DB.query(Tables.TV, projection,
                        BDContract.TV.TV_NAME + " like " + "\'%" + id + "%\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : "")+ " GROUP BY (" + BDContract.TV.TV_NAME+ ")",
                        selectionArgs,
                        null, null, sortOrder);

                break;


            default:
                throw new UnsupportedOperationException("no_support_uri");
        }

        c.setNotificationUri(Content_Resolver, uri);

        return c;
    }


}
