package com.test.movies.data;

import android.net.Uri;

/**
 * Created by hp on 18/3/2017.
 */

public class BDContract {

    interface MovieColumns{
        String MOVIE_ID = "movie_id";
        String MOVIE_CAT = "movie_cat";
        String MOVIE_NAME = "movie_name";
        String MOVIE_POSTER = "movie_poster";
        String MOVIE_OVERVIEW = "movie_overview";
        String MOVIE_RATING = "movie_rating";
        String MOVIE_DATE = "movie_date";

    }

    interface TVColumns{
        String TV_ID = "tv_id";
        String TV_CAT = "tv_cat";
        String TV_NAME = "tv_name";
        String TV_POSTER = "tv_poster";
        String TV_OVERVIEW = "tv_overview";
        String TV_RATING = "tv_rating";
        String TV_DATE = "tv_date";

    }

    public static final String CONTENT_AUTHORITY = "com.test.movies";

    public static final Uri URI_BASE = Uri.parse("content://" + CONTENT_AUTHORITY);


    private static final String ROUTE_MOVIES = "movies";
    private static final String ROUTE_TV = "tv";





    public static final String CONTENT_BASE = "movie.";

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + CONTENT_BASE;

    public static final String CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd."
            + CONTENT_BASE;

    public static String generarMime(String id) {
        if (id != null) {
            return CONTENT_TYPE + id;
        } else {
            return null;
        }
    }

    public static String generarMimeItem(String id) {
        if (id != null) {
            return CONTENT_TYPE_ITEM + id;
        } else {
            return null;
        }
    }

    public static class Moview implements MovieColumns {

        public static final Uri CONTENT_URI =
                URI_BASE.buildUpon().appendPath(ROUTE_MOVIES).build();



        public static String getIdMovies(Uri uri) {
            return uri.getPathSegments().get(1);
        }


        public static Uri createUriMovies(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }


        public static Uri createUriMoviesTypes(String type) {
            return CONTENT_URI.buildUpon().appendPath(type).appendPath("type").build();
        }

        public static Uri createUriMoviesSeacrh(String search) {
            return CONTENT_URI.buildUpon().appendPath(search).appendPath("search").build();
        }

        public static Uri createUriMoviesDelete(String deletecat) {
            return CONTENT_URI.buildUpon().appendPath(deletecat).appendPath("deletecat").build();
        }


    }

    public static class TV implements TVColumns {

        public static final Uri CONTENT_URI =
                URI_BASE.buildUpon().appendPath(ROUTE_TV).build();



        public static String getIdTV(Uri uri) {
            return uri.getPathSegments().get(1);
        }


        public static Uri createUriTV(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }


        public static Uri createUriTVTypes(String type) {
            return CONTENT_URI.buildUpon().appendPath(type).appendPath("type").build();
        }

        public static Uri createUriTVSeacrh(String search) {
            return CONTENT_URI.buildUpon().appendPath(search).appendPath("search").build();
        }

    }
}
