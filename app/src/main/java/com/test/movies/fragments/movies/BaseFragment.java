package com.test.movies.fragments.movies;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.movies.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.test.movies.preferences.PreferenceHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    @BindView(R.id.viewpagerb)
    ViewPager viewpager;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindString(R.string.title_section1)
    public String titleSection1;

    @BindString(R.string.title_section2)
    public String titleSection2;

    @BindString(R.string.title_section3)
    public String titleSection3;

    @BindString(R.string.title_sectionHome1)
    public String titleSectionHome1;

    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        ButterKnife.bind(this,view);
        getActivity().setTitle(titleSectionHome1);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Setting ViewPager for each Tabs
        setupViewPager(viewpager);
        // Set Tabs inside Toolbar
        tabs.setupWithViewPager(viewpager);
    }


    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        BaseFragment.Adapter adapter = new BaseFragment.Adapter(getChildFragmentManager());

        adapter.addFragment(new PopularMoviesFragment(), titleSection1);
        adapter.addFragment(new TopMoviesFragment(), titleSection2);
        adapter.addFragment(new UpcomingMoviesFragment(), titleSection3);

        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentbase = new ArrayList<>();
        private List<String> titleSeccion = new ArrayList<>();



        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentbase.get(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentbase.add(fragment);
            titleSeccion.add (title);

        }


        @Override
        public CharSequence getPageTitle(int position) {
            return titleSeccion.get(position);
        }
    }



}
