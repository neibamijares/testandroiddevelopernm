package com.test.movies.fragments.movies;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.movies.R;
import com.test.movies.adapters.RecyclerViewAdapterMovies;
import com.test.movies.data.BDManager;
import com.test.movies.models.MoviesBd;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopularMoviesFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private List<MoviesBd> movies = null;

    public PopularMoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_tab, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Cursor moviesC = BDManager.MoviesType(getActivity(),"POPULAR");
        Log.e("onResume: ", String.valueOf(moviesC.getCount()));
        movies = new ArrayList<>();
        if (moviesC!=null && moviesC.getCount()>0){
            while(moviesC.moveToNext()){
                MoviesBd mov = new MoviesBd(moviesC);
                movies.add(mov);
            }

            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
            recycler_view.setLayoutManager(layoutManager);

            recycler_view.setHasFixedSize(true);

            RecyclerViewAdapterMovies mAdapter = new RecyclerViewAdapterMovies(getActivity(),movies);
            recycler_view.setAdapter(mAdapter);
        }

    }
}
