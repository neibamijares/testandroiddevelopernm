package com.test.movies.fragments.series;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.movies.R;
import com.test.movies.adapters.RecyclerViewAdapterSeries;
import com.test.movies.data.BDManager;
import com.test.movies.models.TVBd;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopularSeriesFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private List<TVBd> tv = null;

    public PopularSeriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_tab, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Cursor tvc = BDManager.SeriesType(getActivity(),"POPULAR");
        Log.e("onResume: ", String.valueOf(tvc.getCount()));
        tv = new ArrayList<>();
        if (tvc!=null && tvc.getCount()>0){
            while(tvc.moveToNext()){
                TVBd tvs = new TVBd(tvc);
                tv.add(tvs);
            }

            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
            recycler_view.setLayoutManager(layoutManager);

            recycler_view.setHasFixedSize(true);

            RecyclerViewAdapterSeries mAdapter = new RecyclerViewAdapterSeries(getActivity(),tv);
            recycler_view.setAdapter(mAdapter);
        }

    }
}
