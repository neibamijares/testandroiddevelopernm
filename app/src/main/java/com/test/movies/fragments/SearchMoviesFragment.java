package com.test.movies.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.movies.R;
import com.test.movies.adapters.RecyclerViewAdapterMovies;
import com.test.movies.data.BDManager;
import com.test.movies.models.MoviesBd;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchMoviesFragment extends Fragment {


    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private String search;

    private List<MoviesBd> movies = new ArrayList<>();

    public SearchMoviesFragment() {
        // Required empty public constructor
    }

    public static SearchMoviesFragment newInstance(String search) {
        SearchMoviesFragment fragment = new SearchMoviesFragment();
        Bundle args = new Bundle();
        args.putString("SEARCH", search);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            search = getArguments().getString("SEARCH");

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_tab, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Cursor sea = BDManager.MoviesSearch(getActivity(),search);

        if (sea!=null && sea.getCount()>0){
                while(sea.moveToNext()){
                    MoviesBd mov = new MoviesBd(sea);
                    movies.add(mov);
                }

            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
            recycler_view.setLayoutManager(layoutManager);

            recycler_view.setHasFixedSize(true);

            RecyclerViewAdapterMovies mAdapter = new RecyclerViewAdapterMovies(getActivity(),movies);
            recycler_view.setAdapter(mAdapter);
        }

    }
}
