package com.test.movies.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.test.movies.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment
        extends Fragment {

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindString(R.string.title_sectionHome1)
    public String titleSectionHome1;

    @BindString(R.string.title_sectionHome2)
    public String titleSectionHome2;

    @BindString(R.string.app_name)
    public String titleSectionHome;



    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance( ) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        getActivity().setTitle(titleSectionHome);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        // Setting ViewPager for each Tabsc
        setupViewPager(viewpager);
        // Set Tabs inside Toolbar
        tabs.setupWithViewPager(viewpager);

    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());

        adapter.addFragment(new MoviesFragment(), titleSectionHome1);
        adapter.addFragment(new SeriesFragment(), titleSectionHome2);


        viewPager.setAdapter(adapter);
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentbase = new ArrayList<>();
        private List<String> titleSeccion = new ArrayList<>();



        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentbase.get(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentbase.add(fragment);
            titleSeccion.add (title);

        }


        @Override
        public CharSequence getPageTitle(int position) {
            return titleSeccion.get(position);
        }
    }

}
