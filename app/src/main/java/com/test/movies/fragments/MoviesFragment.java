package com.test.movies.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.test.movies.R;
import com.test.movies.adapters.AdapterMovies;
import com.test.movies.data.BDManager;
import com.test.movies.models.CatMovies;
import com.test.movies.models.HeaderoRowMovies;
import com.test.movies.models.MoviesBd;
import com.test.movies.models.ResultCatMovies;
import com.test.movies.preferences.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.test.movies.sync.MoviesInformation.getCatMovies;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {

    @BindView(R.id.progress_Bar)
    ProgressBar progress_Bar;

    @BindView(R.id.movies)
    RecyclerView movies;

    private List<HeaderoRowMovies> data;


    private AdapterMovies adapter;

    private Cursor cursormovies =null;

    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = new ArrayList<>();
        adapter = new AdapterMovies(getActivity(),data);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        progress_Bar.setVisibility(View.VISIBLE);
        Cursor bd = BDManager.AllMovies(getActivity());

        if (PreferenceHelper.hasInternet(getActivity())&& (bd==null || bd.getCount()<=0)){
            popular();
            toprated();
            upcoming();
        }else if ( data.size() == 0 ){
            cursormovies = BDManager.AllMovies(getActivity());

            if (cursormovies!=null && cursormovies.getCount()>0)
            {
                List<ResultCatMovies> rows= new ArrayList<ResultCatMovies>();

                for (int i=0;i<14;i++){

                    Random r = new Random();
                    int i1 = r.nextInt(cursormovies.getCount() - i) + i;

                    ResultCatMovies moviescat;

                    cursormovies.moveToFirst();

                    if (i<4){

                        if(i==0){
                            data.add(HeaderoRowMovies.createHeader("POPULAR"));
                        }else{
                            cursormovies.move(i1);

                            MoviesBd mbd = new MoviesBd(cursormovies);

                            if (mbd.getMovie_cat().equals("POPULAR")){

                                moviescat = new ResultCatMovies();

                                moviescat.setId(Integer.valueOf(mbd.getMovie_id()));
                                moviescat.setTitle(mbd.getMovie_name());
                                moviescat.setPosterPath(mbd.getMovie_poster());
                                moviescat.setOverview(mbd.getMovie_overview());
                                moviescat.setVoteAverage(Double.valueOf(mbd.getMovie_rating()));
                                moviescat.setReleaseDate(mbd.getMovie_date());

                                rows.add(moviescat);
                            }else{
                                --i;
                            }
                        }


                    }else if (i<8)
                    {
                        if(i==4){
                            data.add(HeaderoRowMovies.createRow(rows));
                            rows=null;
                            rows= new ArrayList<ResultCatMovies>();
                            data.add(HeaderoRowMovies.createHeader("TOP"));
                        }
                        else{

                            cursormovies.move(i1);

                            MoviesBd mbd = new MoviesBd(cursormovies);

                            if (mbd.getMovie_cat().equals("TOP")){

                                moviescat = new ResultCatMovies();

                                moviescat.setId(Integer.valueOf(mbd.getMovie_id()));
                                moviescat.setTitle(mbd.getMovie_name());
                                moviescat.setPosterPath(mbd.getMovie_poster());
                                moviescat.setOverview(mbd.getMovie_overview());
                                moviescat.setVoteAverage(Double.valueOf(mbd.getMovie_rating()));
                                moviescat.setReleaseDate(mbd.getMovie_date());

                                rows.add(moviescat);
                            }else{
                                --i;
                            }
                        }

                    }else if (i<13)
                    {

                        if(i==8){
                            data.add(HeaderoRowMovies.createRow(rows));
                            rows= new ArrayList<ResultCatMovies>();
                            data.add(HeaderoRowMovies.createHeader("UPCOMING"));
                        }
                        else{
                            cursormovies.move(i1);

                            MoviesBd mbd = new MoviesBd(cursormovies);

                            if (mbd.getMovie_cat().equals("UPCOMING")){

                                moviescat = new ResultCatMovies();

                                moviescat.setId(Integer.valueOf(mbd.getMovie_id()));
                                moviescat.setTitle(mbd.getMovie_name());
                                moviescat.setPosterPath(mbd.getMovie_poster());
                                moviescat.setOverview(mbd.getMovie_overview());
                                moviescat.setVoteAverage(Double.valueOf(mbd.getMovie_rating()));
                                moviescat.setReleaseDate(mbd.getMovie_date());

                                rows.add(moviescat);

                            }else{
                                --i;
                            }
                        }


                    }else{
                        data.add(HeaderoRowMovies.createRow(rows));
                    }


                }

                cursormovies.close();
            }




        }

        progress_Bar.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        movies.setLayoutManager(linearLayoutManager);
        movies.setHasFixedSize(true);

        movies.setAdapter(adapter);


    }

    public void popular(){

        Observable<CatMovies> observable = getCatMovies(getActivity()).popular(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatMovies>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onNext(CatMovies movies) {
                        Log.e("onNext: ", String.valueOf(movies));
//                        BDManager.MoviesDelete(getActivity(),"POPULAR");
                        data.add(HeaderoRowMovies.createHeader("POPULAR"));

                        List<ResultCatMovies> rows= new ArrayList<ResultCatMovies>();
                        List<MoviesBd> dataBD = new ArrayList<>();

                        for (int i=0;i<movies.getResults().size();i++){
                            MoviesBd bd = new MoviesBd(String.valueOf(movies.getResults().get(i).getId()),
                                    movies.getResults().get(i).getTitle(),
                                    movies.getResults().get(i).getPosterPath(),
                                    movies.getResults().get(i).getOverview(),
                                    String.valueOf(movies.getResults().get(i).getVoteAverage()),
                                    movies.getResults().get(i).getReleaseDate(),"POPULAR");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(movies.getResults().size() - i) + i;
                                rows.add(movies.getResults().get(i1));
                            }

                        }


                        BDManager.SaveMovies(getActivity(),dataBD);

                        data.add(HeaderoRowMovies.createRow(rows));


                        adapter.updateView( );


                    }
                });

    }

    public void toprated(){

        Observable<CatMovies> observable = getCatMovies(getActivity()).topRated(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatMovies>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(CatMovies movies) {
                        Log.e("onNext: ", String.valueOf(movies));
//                        BDManager.MoviesDelete(getActivity(),"TOP");
                        data.add(HeaderoRowMovies.createHeader("TOP"));

                        List<ResultCatMovies> rows= new ArrayList<ResultCatMovies>();
                        List<MoviesBd> dataBD = new ArrayList<>();


                        for (int i=0;i<movies.getResults().size();i++){
                            MoviesBd bd = new MoviesBd(String.valueOf(movies.getResults().get(i).getId()),
                                    movies.getResults().get(i).getTitle(),
                                    movies.getResults().get(i).getPosterPath(),
                                    movies.getResults().get(i).getOverview(),
                                    String.valueOf(movies.getResults().get(i).getVoteAverage()),
                                    movies.getResults().get(i).getReleaseDate(),"TOP");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(movies.getResults().size() - i) + i;
                                rows.add(movies.getResults().get(i1));
                            }

                        }

                        BDManager.SaveMovies(getActivity(),dataBD);

                        data.add(HeaderoRowMovies.createRow(rows));


                        adapter.updateView( );


                    }
                });

    }

    public void upcoming(){

        Observable<CatMovies> observable = getCatMovies(getActivity()).upcoming(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatMovies>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());

                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(CatMovies movies) {
                        Log.e("onNext: ", String.valueOf(movies));
                        progress_Bar.setVisibility(View.GONE);
                        //   BDManager.MoviesDelete(getActivity(),"UPCOMING");
                        data.add(HeaderoRowMovies.createHeader("UPCOMING"));

                        List<ResultCatMovies> rows= new ArrayList<ResultCatMovies>();
                        List<MoviesBd> dataBD = new ArrayList<>();

                        for (int i=0;i<movies.getResults().size();i++){
                            MoviesBd bd = new MoviesBd(String.valueOf(movies.getResults().get(i).getId()),
                                    movies.getResults().get(i).getTitle(),
                                    movies.getResults().get(i).getPosterPath(),
                                    movies.getResults().get(i).getOverview(),
                                    String.valueOf(movies.getResults().get(i).getVoteAverage()),
                                    movies.getResults().get(i).getReleaseDate(),"UPCOMING");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(movies.getResults().size() - i) + i;
                                rows.add(movies.getResults().get(i1));
                            }

                        }



                        data.add(HeaderoRowMovies.createRow(rows));


                        adapter.updateView( );

                        BDManager.SaveMovies(getActivity(),dataBD);

                    }
                });

    }
}