package com.test.movies.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.test.movies.R;
import com.test.movies.adapters.AdapterTV;
import com.test.movies.data.BDManager;
import com.test.movies.models.CatTV;
import com.test.movies.models.DetailTV.HeaderoRowTV;
import com.test.movies.models.ResultCatTV;
import com.test.movies.models.TVBd;
import com.test.movies.preferences.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.test.movies.sync.TVInformation.getCatTV;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeriesFragment extends Fragment {


    @BindView(R.id.progress_Bar)
    ProgressBar progress_Bar;

    @BindView(R.id.series)
    RecyclerView series;

    private List<HeaderoRowTV> data = new ArrayList<>();

    private AdapterTV adapter;



    public SeriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new AdapterTV(getActivity(),data);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_series, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        progress_Bar.setVisibility(View.VISIBLE);
        Cursor bd = BDManager.AllTV(getActivity());
        if (PreferenceHelper.hasInternet(getActivity())&& (bd==null || bd.getCount()<=0)){
            popular();
            toprated();
            upcoming();
        }else if ( data.size() == 0 ){
            Cursor tv = BDManager.AllTV(getActivity());

            if (tv!=null && tv.getCount()>0)
            {
                List<ResultCatTV> rows= new ArrayList<ResultCatTV>();

                for (int i=0;i<14;i++){

                    Random r = new Random();
                    int i1 = r.nextInt(tv.getCount() - i) + i;

                    ResultCatTV tvcat = new ResultCatTV();

                    tv.moveToFirst();

                    if (i<4){

                        if(i==0){
                            data.add(HeaderoRowTV.createHeader("POPULAR"));
                        }else{
                            tv.move(i1);

                            TVBd mbd = new TVBd(tv);

                            if (mbd.getTv_cat().equals("POPULAR")){

                                tvcat = new ResultCatTV();

                                tvcat.setId(Integer.valueOf(mbd.getTv_id()));
                                tvcat.setName(mbd.getTv_name());
                                tvcat.setPosterPath(mbd.getTv_poster());
                                tvcat.setOverview(mbd.getTv_overview());
                                tvcat.setVoteAverage(Double.valueOf(mbd.getTv_rating()));
                                tvcat.setFirstAirDate(mbd.getTv_date());

                                rows.add(tvcat);
                            }else{
                                --i;
                            }
                        }


                    }else if (i<8)
                    {
                        if(i==4){
                            data.add(HeaderoRowTV.createRow(rows));
                            rows=null;
                            rows= new ArrayList<ResultCatTV>();
                            data.add(HeaderoRowTV.createHeader("TOP"));
                        }
                        else{

                            tv.move(i1);

                            TVBd mbd = new TVBd(tv);

                            if (mbd.getTv_cat().equals("TOP")){

                                tvcat = new ResultCatTV();

                                tvcat.setId(Integer.valueOf(mbd.getTv_id()));
                                tvcat.setName(mbd.getTv_name());
                                tvcat.setPosterPath(mbd.getTv_poster());
                                tvcat.setOverview(mbd.getTv_overview());
                                tvcat.setVoteAverage(Double.valueOf(mbd.getTv_rating()));
                                tvcat.setFirstAirDate(mbd.getTv_date());

                                rows.add(tvcat);
                            }else{
                                --i;
                            }
                        }

                    }else if (i<13)
                    {

                        if(i==8){
                            data.add(HeaderoRowTV.createRow(rows));
                            rows=null;
                            rows= new ArrayList<ResultCatTV>();
                            data.add(HeaderoRowTV.createHeader("UPCOMING"));
                        }
                        else{
                            tv.move(i1);

                            TVBd mbd = new TVBd(tv);

                            if (mbd.getTv_cat().equals("UPCOMING")){

                                tvcat = new ResultCatTV();

                                tvcat.setId(Integer.valueOf(mbd.getTv_id()));
                                tvcat.setName(mbd.getTv_name());
                                tvcat.setPosterPath(mbd.getTv_poster());
                                tvcat.setOverview(mbd.getTv_overview());
                                tvcat.setVoteAverage(Double.valueOf(mbd.getTv_rating()));
                                tvcat.setFirstAirDate(mbd.getTv_date());

                                rows.add(tvcat);
                            }else{
                                --i;
                            }
                        }


                    }else{
                        data.add(HeaderoRowTV.createRow(rows));
                    }


                }


            }

            tv.close();



        }
        progress_Bar.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        series.setLayoutManager(linearLayoutManager);
        series.setHasFixedSize(true);

        series.setAdapter(adapter);

    }

    public void toprated(){

        Observable<CatTV> observable = getCatTV(getActivity()).topRated(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatTV>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(CatTV TV) {
                        Log.e("onNext: ", String.valueOf(TV));

                        data.add(HeaderoRowTV.createHeader("TOP"));

                        List<ResultCatTV> rows= new ArrayList<ResultCatTV>();
                        List<TVBd> dataBD = new ArrayList<>();

                        for (int i=0;i<TV.getResults().size();i++){
                            TVBd bd = new TVBd(String.valueOf(TV.getResults().get(i).getId()),
                                    TV.getResults().get(i).getName(),
                                    TV.getResults().get(i).getPosterPath(),
                                    TV.getResults().get(i).getOverview(),
                                    String.valueOf(TV.getResults().get(i).getVoteAverage()),
                                    TV.getResults().get(i).getFirstAirDate(),"TOP");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(TV.getResults().size() - i) + i;
                                rows.add(TV.getResults().get(i1));
                            }

                        }
                        BDManager.SaveTV(getActivity(),dataBD);

                        data.add(HeaderoRowTV.createRow(rows));


                        adapter.updateView( );

                    }
                });

    }

    public void upcoming(){

        Observable<CatTV> observable = getCatTV(getActivity()).airingToday(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatTV>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(CatTV TV) {
                        Log.e("onNext: ", String.valueOf(TV));

                        data.add(HeaderoRowTV.createHeader("UPCOMING"));

                        List<ResultCatTV> rows= new ArrayList<ResultCatTV>();
                        List<TVBd> dataBD = new ArrayList<>();

                        for (int i=0;i<TV.getResults().size();i++){
                            TVBd bd = new TVBd(String.valueOf(TV.getResults().get(i).getId()),
                                    TV.getResults().get(i).getName(),
                                    TV.getResults().get(i).getPosterPath(),
                                    TV.getResults().get(i).getOverview(),
                                    String.valueOf(TV.getResults().get(i).getVoteAverage()),
                                    TV.getResults().get(i).getFirstAirDate(),"UPCOMING");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(TV.getResults().size() - i) + i;
                                rows.add(TV.getResults().get(i1));
                            }

                        }

                        BDManager.SaveTV(getActivity(),dataBD);
                        data.add(HeaderoRowTV.createRow(rows));


                        adapter.updateView( );

                    }
                });

    }

    public void popular(){

        Observable<CatTV> observable = getCatTV(getActivity()).popular(getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CatTV>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(CatTV TV) {
                        Log.e("onNext: ", String.valueOf(TV));
                        progress_Bar.setVisibility(View.GONE);

                        data.add(HeaderoRowTV.createHeader("POPULAR"));

                        List<ResultCatTV> rows= new ArrayList<ResultCatTV>();
                        List<TVBd> dataBD = new ArrayList<>();

                        for (int i=0;i<TV.getResults().size();i++){
                            TVBd bd = new TVBd(String.valueOf(TV.getResults().get(i).getId()),
                                    TV.getResults().get(i).getName(),
                                    TV.getResults().get(i).getPosterPath(),
                                    TV.getResults().get(i).getOverview(),
                                    String.valueOf(TV.getResults().get(i).getVoteAverage()),
                                    TV.getResults().get(i).getFirstAirDate(),"POPULAR");

                            dataBD.add(bd);

                            if (i<3){
                                Random r = new Random();
                                int i1 = r.nextInt(TV.getResults().size() - i) + i;
                                rows.add(TV.getResults().get(i1));
                            }

                        }


                        data.add(HeaderoRowTV.createRow(rows));


                        adapter.updateView( );

                        BDManager.SaveTV(getActivity(),dataBD);

                    }
                });

    }



}