package com.test.movies.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.test.movies.R;

import butterknife.BindString;

public class PreferenceHelper {


    public static String prefName = "Preferences";


    public static void clearPreferences(Context context) {
        SharedPreferences sharedPref  = context.getSharedPreferences(PreferenceHelper.prefName, 0);
        sharedPref .edit().clear().apply();
    }


    public static void setInternet(Context context, boolean v) {
        SharedPreferences sharedPref  = context.getSharedPreferences(PreferenceHelper.prefName, 0);
        SharedPreferences.Editor editor = sharedPref .edit();
        editor.putBoolean("inter", v);
        editor.apply();
    }

    public static boolean hasInternet(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PreferenceHelper.prefName, 0);
        return settings.getBoolean("inter", false);
    }

}
