package com.test.movies.sync;

import android.content.Context;

import retrofit2.http.Path;
import rx.Observable;

import com.test.movies.R;
import com.test.movies.models.CatMovies;
import com.test.movies.models.DetailMovies.DetailMovie;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;




public class MoviesInformation {

    public interface MoviesInformationInterface {

        @GET("movie/popular")
        Observable<CatMovies> popular(@Query("api_key") String key,
                                      @Query("language") String language);


        @GET("movie/top_rated")
        Observable<CatMovies> topRated(@Query("api_key") String key,
                                      @Query("language") String language);

        @GET("movie/upcoming")
        Observable<CatMovies> upcoming(@Query("api_key") String key,
                                       @Query("language") String language);

        @GET("movie/{movie_id}")
        Observable<DetailMovie> detailMovie(@Path("movie_id") String id, @Query("api_key") String key,
                                            @Query("language") String language);

    }

    private static MoviesInformationInterface adapter;

    public static MoviesInformationInterface getCatMovies(final Context context) {
        if (adapter == null) {
            // Log
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            // OkHttpClient
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Content-Type:", "application/json;charset=utf-8")
                            .build();

                    // Customize or return the response
                    return chain.proceed(request);
                }
            });

            // Add the token
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Authorization", context.getString(R.string.token))
                            .build();
                    // Customize or return the response
                    return chain.proceed(request);

                }
            });



            // add logging as last interceptor
            httpClient.addInterceptor(logging);

            OkHttpClient client = httpClient.build();
            Retrofit restAdapter = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(context.getString(R.string.endpoint))
                    .client(client)
                    .build();

            adapter = restAdapter.create(MoviesInformationInterface.class);
        }

        return adapter;
    }

}
