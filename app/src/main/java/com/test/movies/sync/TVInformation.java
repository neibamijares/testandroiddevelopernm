package com.test.movies.sync;

import android.content.Context;

import com.test.movies.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by hp on 18/3/2017.
 */

        import android.content.Context;
        import rx.Observable;

        import com.test.movies.R;
        import com.test.movies.models.CatMovies;
import com.test.movies.models.CatTV;
import com.test.movies.models.DetailTV.DetailTV;


import java.io.IOException;

        import okhttp3.Interceptor;
        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import okhttp3.Response;
        import okhttp3.logging.HttpLoggingInterceptor;
        import retrofit2.Retrofit;
        import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
        import retrofit2.converter.gson.GsonConverterFactory;
        import retrofit2.http.GET;
        import retrofit2.http.Query;




public class TVInformation {

    public interface TVInformationInterface {

        @GET("tv/popular")
        Observable<CatTV> popular(@Query("api_key") String key,
                                  @Query("language") String language);


        @GET("tv/top_rated")
        Observable<CatTV> topRated(@Query("api_key") String key,
                                       @Query("language") String language);

        @GET("tv/airing_today")
        Observable<CatTV> airingToday(@Query("api_key") String key,
                                       @Query("language") String language);

        @GET("tv/{tv_id}")
        Observable<DetailTV> detailTV(@Path("tv_id") String id, @Query("api_key") String key,
                                      @Query("language") String language);

    }

    private static TVInformationInterface adapter;

    public static TVInformationInterface getCatTV(final Context context) {
        if (adapter == null) {
            // Log
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            // OkHttpClient
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Content-Type:", "application/json;charset=utf-8")
                            .build();

                    // Customize or return the response
                    return chain.proceed(request);
                }
            });

            // Add the token
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Authorization", context.getString(R.string.token))
                            .build();
                    // Customize or return the response
                    return chain.proceed(request);

                }
            });



            // add logging as last interceptor
            httpClient.addInterceptor(logging);

            OkHttpClient client = httpClient.build();
            Retrofit restAdapter = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(context.getString(R.string.endpoint))
                    .client(client)
                    .build();

            adapter = restAdapter.create(TVInformationInterface.class);
        }

        return adapter;
    }

}
