package com.test.movies.activities;



import android.content.Context;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.v4.app.FragmentManager;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.test.movies.R;
import com.test.movies.fragments.HomeFragment;
import com.test.movies.fragments.SearchMoviesFragment;
import com.test.movies.fragments.SearchSeriesFragment;
import com.test.movies.fragments.movies.BaseFragment;
import com.test.movies.fragments.series.BaseSFragment;
import com.test.movies.preferences.PreferenceHelper;




public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private SearchView mSearchView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        setToolbar();

        isNetworkAvailable( );

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setQueryHint("Search...");
        mSearchView.setOnQueryTextListener(this);
        return true;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //Toast.makeText(this, query, Toast.LENGTH_SHORT).show();

        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();

        switch (fragmentTag) {
            case "HomeFragment":

                ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
                Log.e("onQueryTextSubmit: ", String.valueOf(viewpager.getCurrentItem()));

                if (viewpager.getCurrentItem()==0){
                    SearchMoviesFragment fragmento = (SearchMoviesFragment) getSupportFragmentManager().findFragmentById(R.id.searchMovies);
                    if (fragmento==null)
                    {
                        fragmento = SearchMoviesFragment.newInstance(query);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, fragmento)
                                .addToBackStack("SEARCHFRAGMENT")
                                .commit();
                    }
                }else
                {

                    SearchSeriesFragment fragmento = (SearchSeriesFragment) getSupportFragmentManager().findFragmentById(R.id.searchSeries);
                    if (fragmento==null)
                    {
                        fragmento = SearchSeriesFragment.newInstance(query);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, fragmento)
                                .addToBackStack("SEARCHFRAGMENT")
                                .commit();
                    }

                }

                break;


            case "MOVIES":

                SearchMoviesFragment fragmento = (SearchMoviesFragment) getSupportFragmentManager().findFragmentById(R.id.searchMovies);
                if (fragmento==null)
                {
                    fragmento = SearchMoviesFragment.newInstance(query);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragmento)
                            .addToBackStack("SEARCHFRAGMENT")
                            .commit();
                }

                break;

            case "SERIES":

                SearchSeriesFragment fragmentos = (SearchSeriesFragment) getSupportFragmentManager().findFragmentById(R.id.searchSeries);
                if (fragmentos==null)
                {
                    fragmentos = SearchSeriesFragment.newInstance(query);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragmentos)
                            .addToBackStack("SEARCHFRAGMENT")
                            .commit();
                }

                break;

        }

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
       // Toast.makeText(this, "Searching for " + newText, Toast.LENGTH_LONG).show();
        if (newText.equals("")) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();

            if (fragmentTag !="HomeFragment") {
                onBackPressed();

            }
        }
        return false;
    }



    @Override
    protected void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .addToBackStack("HomeFragment")
                .commit();
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();

            switch (fragmentTag) {
                case "HomeFragment":
                    finish();
                    break;


                default:
                    super.onBackPressed();
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (item.getItemId()) {
            case R.id.nav_home:

                fragmentManager.beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .addToBackStack("HomeFragment")
                        .commit();
                break;
            case R.id.nav_movies:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new BaseFragment())
                        .addToBackStack("MOVIES")
                        .commit();
               break;
            case R.id.nav_series:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new BaseSFragment())
                        .addToBackStack("SERIES")
                        .commit();
                break;


        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }




    public boolean isNetworkAvailable( ) {

        PreferenceHelper.clearPreferences(this);

        ConnectivityManager connectivityManager
                = (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            getApplicationContext().deleteDatabase(getString(R.string.namebd));
            PreferenceHelper.setInternet(MainActivity.this, true);
            return true;
        }else{
            PreferenceHelper.setInternet(MainActivity.this, false);
            return false;
        }
    }



}
