package com.test.movies.activities;

import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.movies.R;
import com.test.movies.data.BDManager;
/*import com.test.movies.models.DetailMovies.DetailMovie;
import com.test.movies.models.DetailTV.DetailTV;*/
import com.test.movies.models.MoviesBd;
import com.test.movies.models.TVBd;

import butterknife.BindView;
import butterknife.ButterKnife;
/*import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


import static com.test.movies.sync.MoviesInformation.getCatMovies;
import static com.test.movies.sync.TVInformation.getCatTV;*/

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.detail)
    TextView detail;

    @BindView(R.id.progress_Bar)
    ProgressBar progress_Bar;

    @BindView(R.id.average)
    TextView average;

    @BindView(R.id.release)
    TextView release;


    private int id;
    private String type;
    private CollapsingToolbarLayout collapsingToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);


        id = getIntent().getIntExtra("id", 0);
        type = getIntent().getStringExtra("type");

        progress_Bar.setVisibility(View.VISIBLE);
        if (type.equals("MOVIES"))
        {

            //movieDetail(id);

            Cursor movie = BDManager.MoviesByID(this, String.valueOf(id));

            if (movie!=null && movie.getCount()>0)
            {

                movie.moveToFirst();

                MoviesBd mov = new MoviesBd(movie);

                collapsingToolbar.setTitle(mov.getMovie_name());

                collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                collapsingToolbar.setExpandedTitleTextColor(ColorStateList.valueOf(Color.TRANSPARENT));

                title.setText(mov.getMovie_name());
                detail.setText(mov.getMovie_overview());
                average.setText("Rating: "+ mov.getMovie_rating());
                release.setText(mov.getMovie_date());
                Picasso.with(this).load(getString(R.string.imageurl)+mov.getMovie_poster()).placeholder(R.drawable.movie).into(image);

            }

            progress_Bar.setVisibility(View.GONE);

        }else
        {

            Cursor tv = BDManager.TVByID(this, String.valueOf(id));

            if (tv!=null && tv.getCount()>0)
            {

                tv.moveToFirst();

                TVBd tvd = new TVBd(tv);

                collapsingToolbar.setTitle(tvd.getTv_name());

                collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                collapsingToolbar.setExpandedTitleTextColor(ColorStateList.valueOf(Color.TRANSPARENT));

                title.setText(tvd.getTv_name());
                detail.setText(tvd.getTv_overview());
                average.setText("Rating: "+ tvd.getTv_rating());
                release.setText(tvd.getTv_date());
                Picasso.with(this).load(getString(R.string.imageurl)+tvd.getTv_poster()).placeholder(R.drawable.movie).into(image);

            }

            progress_Bar.setVisibility(View.GONE);
        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*Se comenta motivado a que el detalle se mostrará de acuerdo a la información almacenada en la BD */
   /* public void movieDetail(int id){

        Observable<DetailMovie> observable = getCatMovies(this).detailMovie(String.valueOf(id), getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailMovie>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(DetailMovie movie) {

                        progress_Bar.setVisibility(View.GONE);
                        collapsingToolbar.setTitle(movie.getTitle());

                        title.setText(movie.getTitle());
                        detail.setText(movie.getOverview());
                        average.setText("Rating: "+String.valueOf(movie.getVoteAverage()));
                        release.setText(movie.getReleaseDate());
                        Picasso.with(DetailActivity.this).load(getString(R.string.imageurl)+movie.getPosterPath()).placeholder(R.drawable.movie).into(image);

                    }
                });
    }

    public void tvDetail(int id){
        Observable<DetailTV> observable = getCatTV(this).detailTV(String.valueOf(id), getString(R.string.apikey),getString(R.string.language));
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailTV>() {

                    @Override
                    public void onCompleted() {
                        // handle completed

                    }

                    @Override
                    public void onError(Throwable e) {
                        // handle error
                        Log.e("onError", e.getMessage());
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            int code = response.code();
                            progress_Bar.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNext(DetailTV TV) {
                        Log.e("onNext: ", String.valueOf(TV));

                        progress_Bar.setVisibility(View.GONE);
                        collapsingToolbar.setTitle(TV.getName());

                        title.setText(TV.getName());
                        detail.setText(TV.getOverview());
                        average.setText("Rating: "+String.valueOf(TV.getVoteAverage()));
                        release.setText(TV.getFirstAirDate());
                        Picasso.with(DetailActivity.this).load(getString(R.string.imageurl)+TV.getPosterPath()).placeholder(R.drawable.movie).into(image);

                    }
                });
    }*/
}
