package com.test.movies.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.movies.R;
import com.test.movies.activities.DetailActivity;
import com.test.movies.activities.MainActivity;
import com.test.movies.models.TVBd;

import java.util.List;

/**
 * Created by hp on 19/3/2017.
 */

public class RecyclerViewAdapterSeries extends RecyclerView.Adapter<RecyclerViewAdapterSeries.ViewHolder> {

    private List<TVBd> mDataset;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;
        public ImageView image;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.name);
            image = (ImageView) v.findViewById(R.id.image);
        }
    }


    public RecyclerViewAdapterSeries(Context context, List<TVBd> myDataset) {
        this.mDataset = myDataset;
        this.context = context;
    }


    @Override
    public RecyclerViewAdapterSeries.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_card, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.mTextView.setText(mDataset.get(position).getTv_name());

        Picasso.with(context).load(context.getString(R.string.imageurl)+mDataset.get(position).getTv_poster()).placeholder(R.drawable.movie).into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDetail(Integer.parseInt(mDataset.get(position).getTv_id()));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void callDetail(int id)
    {
        if (context instanceof MainActivity) {
            MainActivity feeds = (MainActivity) context;

            Intent intent = new Intent(feeds, DetailActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("type", "TV");
            feeds.startActivity(intent);

        }
    }
}
