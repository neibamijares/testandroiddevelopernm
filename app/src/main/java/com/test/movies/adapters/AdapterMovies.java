package com.test.movies.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.movies.R;
import com.test.movies.activities.DetailActivity;
import com.test.movies.activities.MainActivity;
import com.test.movies.models.HeaderoRowMovies;

import java.util.List;


public class AdapterMovies extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<HeaderoRowMovies> mData;
    private Context context;


    private List<View> views;

    public AdapterMovies(Context context, List<HeaderoRowMovies> data) {
        this.mData = data;
        this.context = context;

    }


    @Override
    public int getItemCount() {
        int size = 0;
        if(mData!=null){
            size=mData.size();
        }
        return size;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        HeaderoRowMovies item = mData.get(position);
        if(!item.isRow()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_header_list, parent, false);
            return new HeaderViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_row_list, parent, false);
            return new RowViewHolder(v);
        }
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageV1;
        private ImageView imageV2;
        private ImageView imageV3;
        public RowViewHolder(View itemView) {
            super(itemView);
            imageV1 = (ImageView) itemView.findViewById(R.id.image1);
            imageV2 = (ImageView) itemView.findViewById(R.id.image2);
            imageV3 = (ImageView) itemView.findViewById(R.id.image3);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.header);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HeaderoRowMovies item = mData.get(position);
        if(item.isRow()) {
            RowViewHolder h = (RowViewHolder) holder;
            //h.textView.setText(item.getRows().getOriginalTitle());

            Picasso.with(context).load(context.getString(R.string.imageurl)+item.getRows().get(0).getPosterPath()).placeholder(R.drawable.movie).into(h.imageV1);
            Picasso.with(context).load(context.getString(R.string.imageurl)+item.getRows().get(1).getPosterPath()).placeholder(R.drawable.movie).into(h.imageV2);
            Picasso.with(context).load(context.getString(R.string.imageurl)+item.getRows().get(2).getPosterPath()).placeholder(R.drawable.movie).into(h.imageV3);


            h.imageV1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDetail(item.getRows().get(0).getId());
                }
            });

            h.imageV2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDetail(item.getRows().get(1).getId());
                }
            });

            h.imageV3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDetail(item.getRows().get(2).getId());
                }
            });



        } else {
            HeaderViewHolder h = (HeaderViewHolder) holder;
            h.textView.setText(item.getHeader());
        }
    }

    public void updateView( ) {
        notifyDataSetChanged();
    }


    public void callDetail(int id)
    {
        if (context instanceof MainActivity) {
            MainActivity feeds = (MainActivity) context;

            Intent intent = new Intent(feeds, DetailActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("type", "MOVIES");
            feeds.startActivity(intent);

        }
    }


}
