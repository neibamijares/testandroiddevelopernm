package com.test.movies.activities;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void negativa() throws Exception {
        assert(5<1);
    }
    @Test
    public void positiva() throws Exception {
        assert(5>1);
    }

    @Test
    public void isNetworkAvailable( ) {
        MainActivity main = new MainActivity();
        boolean result =main.isNetworkAvailable();
        assert(result);
    }


}